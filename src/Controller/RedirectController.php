<?php
/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 2/20/17
 * Time: 4:17 PM
 */

namespace Drupal\redirect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class RedirectController extends ControllerBase {

    public function getData() {

        $db = \Drupal::database();
        $result = $db->select('tv_redirecting', 'e')->fields('e')->orderBy('id', 'DESC')->execute()->fetchObject();

        if ($result) {
            return new JsonResponse([
                'id'   => $result->id,
                'time' => $result->time,
                'url'  => $result->url,
            ]);
        }

        return new JsonResponse([]);
    }
}